<?php

/**
 * This file is part of the Company PHP Library.
 *
 * @copyright © Company. All rights reserved.
 */

namespace Company\SystemInfo\Linux\Proc;

/**
 * Wrapper around /proc/cpuinfo data.
 */
class Cpuinfo extends AbstractProcWrapper {
	/**
	 * @var string Path to cpuinfo file.
	 */
	const CPUINFO_PATH = '/proc/cpuinfo';

	/**
	 * Object constructor.
	 *
	 * @param string|null $path
	 *   (optional) The path to the meminfo file to be parsed. The default is
	 *   CPUINFO_PATH.
	 *
	 * @return self
	 */
	public function __construct($path = null) {
		$this->path = $path ?? static::CPUINFO_PATH;
		$this->update();
	}

	/**
	 * {@inheritdoc}
	 */
	public function update() {
		if ( ! file_exists($this->path) ) {
			throw new \RuntimeException("No such file: {$this->path}");
		}

		$this->text = trim(file_get_contents($this->path));
		$lines      = explode("\n", $this->text);
		$cpu        = 0;

		if ( count($lines) < 1 || strpos($this->text, ':') === false ) {
			throw new \RuntimeException("Illegal file contents: {$this->path}");
		}

		foreach ( $lines as $line ) {
			$line = trim($line);

			if ( $line === '' ) {
				$cpu++;
				continue;
			}

			$pair = preg_split('/\s*:\s+/', $line, 2, PREG_SPLIT_NO_EMPTY);

			if ( count($pair) !== 2 ) {
				continue;
			}

			$key   = $pair[0];
			$value = $pair[1];

			switch ( $key ) {
				case 'flags':
				case 'power management':
					$value = explode(' ', $value);
					break;
				case 'processor':
				case 'cpu family':
				case 'model':
				case 'stepping':
				case 'physical id':
				case 'siblings':
				case 'core id':
				case 'cpu cores':
				case 'apicid':
				case 'initial apicid':
				case 'cpuid level':
				case 'clflush size':
				case 'cache_alignment':
					$value = (int) $value;
					break;
				case 'cpu MHz':
				case 'bogomips':
					$value = (float) $value;
					break;
			}

			if ( $value === 'yes' || $value === 'true' ) {
				$value = true;
			} elseif ( $value === 'no' || $value === 'false' ) {
				$value = false;
			}

			$this->data[$cpu][$key] = $value;
		}

		foreach ( $this->data as $data ) {
			$keys = [
				'processor',
				'vendor_id',
				'cpu family',
				'model',
				'model name',
				'stepping',
				'cpu MHz',
				'cache size',
				'physical id',
				'siblings',
				'core id',
				'cpu cores',
				'flags',
				'bogomips',
				'power management',
			];

			foreach ( $keys as $key ) {
				$data[$key] = $data[$key] ?? null;
			}
		}

		return $this;
	}

	/**
	 * {@inheritdoc}
	 *
	 * @param int $cpu
	 *   (optional) The CPU ID to get data from.
	 */
	public function get($key, $cpu = 0) {
		return $this->data[$cpu][$key] ?? null;
	}

	/**
	 * {@inheritdoc}
	 */
	public function toCollection() {
		return $this->collection;
	}

	/**
	 * Returns the number of physical CPUs.
	 *
	 * @return int
	 */
	public function getPhysicalCPUs() {
		$ids = [];

		foreach ( $this->data as $cpu ) {
			$ids[$cpu['physical id']] = true;
		}

		return count($ids);
	}

	/**
	 * Returns the number of logical CPUs (cores).
	 *
	 * @return int
	 */
	public function getLogicalCPUs() {
		return count($this->data);
	}
}

