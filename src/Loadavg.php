<?php

/**
 * This file is part of the Company PHP Library.
 *
 * @copyright © Company. All rights reserved.
 */

namespace Company\SystemInfo\Linux\Proc;

/**
 * Wrapper around /proc/loadavg data.
 */
class Loadavg extends AbstractProcWrapper {
	/**
	 * @var string Path to loadavg file.
	 */
	const LOADAVG_PATH = '/proc/loadavg';

	/**
	 * Object constructor.
	 *
	 * @param string|null $path
	 *   (optional) The path to the loadavg file to be parsed. The default is
	 *   LOADAVG_PATH.
	 *
	 * @return self
	 */
	public function __construct($path = null) {
		$this->path = $path ?? static::LOADAVG_PATH;
		$this->update();
	}

	/**
	 * {@inheritdoc}
	 */
	public function update() {
		if ( ! file_exists($this->path) ) {
			throw new \RuntimeException("No such file: {$this->path}");
		}

		$this->text = trim(file_get_contents($this->path));
		$data       = explode(' ', $this->text);

		if ( count($data) !== 5 ) {
			throw new \RuntimeException("Illegal file contents: {$this->path}");
		}

		$this->data = [
			'1MinuteAverage'   => (float) $data[0],
			'5MinuteAverage'   => (float) $data[1],
			'10MinuteAverage'  => (float) $data[2],
			'runningProcesses' => (int) explode('/', $data[3])[0],
			'totalProcesses'   => (int) explode('/', $data[3])[1],
			'lastPID'          => (int) $data[4],
		];

		return $this;
	}

	/**
	 * Returns 1-minute load average.
	 *
	 * @return float
	 */
	public function get1MinuteAverage() {
		return $this->get('1MinuteAverage');
	}

	/**
	 * Returns 5-minute load average.
	 *
	 * @return float
	 */
	public function get5MinuteAverage() {
		return $this->get('5MinuteAverage');
	}

	/**
	 * Returns 10-minute load average.
	 *
	 * @return float
	 */
	public function get10MinuteAverage() {
		return $this->get('10MinuteAverage');
	}

	/**
	 * Returns number of currently running processes.
	 *
	 * @return int
	 */
	public function getRunningProcesses() {
		return $this->get('runningProcesses');
	}

	/**
	 * Returns total number of processes.
	 *
	 * @return int
	 */
	public function getTotalProcesses() {
		return $this->get('totalProcesses');
	}

	/**
	 * Returns last used PID.
	 *
	 * @return int
	 */
	public function getLastPID() {
		return $this->get('lastPID');
	}
}

