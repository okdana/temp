<?php

/**
 * This file is part of the Company PHP Library.
 *
 * @copyright © Company. All rights reserved.
 */

namespace Company\SystemInfo\Linux\Proc;

/**
 * Wrapper around procfs file data.
 */
abstract class AbstractProcWrapper implements \ArrayAccess, \Countable {
	protected $path;
	protected $text;
	protected $data = [];

	/**
	 * Updates instance data from the supplied file.
	 *
	 * @return self
	 *
	 * @throws \RuntimeException if $this->path is non-existent.
	 * @throws \RuntimeException if $this->path contains unexpected data.
	 */
	abstract public function update();

	/**
	 * Returns the value for the specified key.
	 *
	 * @param mixed $key
	 *   The key for which a value should be returned.
	 *
	 * @return mixed
	 */
	public function get($key) {
		if ( is_string($key) ) {
			$key = strtolower($key);

			foreach ( $this->data as $k => $v ) {
				if ( $key === strtolower($k) ) {
					return $v;
				}
			}
			return null;
		}
		return $this->data[$key] ?? null;
	}

	/**
	 * Returns instance data as an array.
	 *
	 * @return array
	 */
	public function toArray() {
		return $this->data;
	}

	/**
	 * Returns instance data as a collection.
	 *
	 * @return array
	 */
	public function toCollection() {
		return new \Company\Collection\Collection($this->data);
	}

	/**
	 * Returns string representation of the instance (wrapper for toString()).
	 *
	 * @return string
	 */
	public function toString() {
		return $this->text;
	}

	/**
	 * Returns string representation of the instance (wrapper for toString()).
	 *
	 * @return string
	 */
	public function __toString() {
		return $this->toString();
	}

	/**
	 * Returns whether a data element exists at the specified offset.
	 *
	 * @see \ArrayAccess
	 *
	 * @param mixed $offset
	 *
	 * @return bool
	 */
	public function offsetExists($offset) {
		return isset($this->data[$offset]);
	}

	/**
	 * Returns the data element at the specified offset.
	 *
	 * @see \ArrayAccess
	 *
	 * @param mixed $offset
	 *
	 * @return mixed
	 */
	public function offsetGet($offset) {
		return isset($this->data[$offset]) ? $this->data[$offset] : null;
	}

	/**
	 * No implemented.
	 *
	 * @see \ArrayAccess
	 *
	 * @param mixed $offset
	 * @param mixed $value
	 *
	 * @return void
	 *
	 * @throws \BadMethodCallException in all cases.
	 */
	public function offsetSet($offset, $value) {
		throw new \BadMethodCallException('Object is immutable');
	}

	/**
	 * No implemented.
	 *
	 * @see \ArrayAccess
	 *
	 * @param mixed $offset
	 *
	 * @return void
	 *
	 * @throws \BadMethodCallException in all cases.
	 */
	public function offsetUnset($offset) {
		throw new \BadMethodCallException('Object is immutable');
	}

	/**
	 * Returns the count of data elements.
	 *
	 * @see \Countable
	 *
	 * @return int
	 */
	public function count() {
		return count($this->data);
	}
}

