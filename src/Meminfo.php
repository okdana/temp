<?php

/**
 * This file is part of the Company PHP Library.
 *
 * @copyright © Company. All rights reserved.
 */

namespace Company\SystemInfo\Linux\Proc;

/**
 * Wrapper around /proc/meminfo data.
 */
class Meminfo extends AbstractProcWrapper {
	/**
	 * @var string Path to meminfo file.
	 */
	const MEMINFO_PATH = '/proc/meminfo';

	/**
	 * Object constructor.
	 *
	 * @param string|null $path
	 *   (optional) The path to the meminfo file to be parsed. The default is
	 *   MEMINFO_PATH.
	 *
	 * @return self
	 */
	public function __construct($path = null) {
		$this->path = $path ?? static::MEMINFO_PATH;
		$this->update();
	}

	/**
	 * {@inheritdoc}
	 */
	public function update() {
		if ( ! file_exists($this->path) ) {
			throw new \RuntimeException("No such file: {$this->path}");
		}

		$this->text = trim(file_get_contents($this->path));
		$lines      = explode("\n", $this->text);

		if ( count($lines) < 1 || ! strpos($this->text, ':') === false ) {
			throw new \RuntimeException("Illegal file contents: {$this->path}");
		}

		foreach ( $lines as $line ) {
			$line = trim($line);
			$pair = preg_split('/:\s+/', $line, 2, PREG_SPLIT_NO_EMPTY);

			if ( count($pair) !== 2 ) {
				continue;
			}

			$key = $pair[0];

			if ( strpos($pair[1], ' kB') !== false ) {
				$value = rtrim($pair[1], ' kB');
				$value = $value * 1024;
			} else {
				$value = ctype_digit($value) ? (int) $value : $value;
			}

			$this->data[$key] = $value;
		}

		return $this;
	}

	/**
	 * Returns MemTotal value in bytes.
	 *
	 * @return int
	 */
	public function getMemTotal() {
		return $this->get('MemTotal');
	}

	/**
	 * Returns MemFree value in bytes.
	 *
	 * @return int
	 */
	public function getMemFree() {
		return $this->get('MemFree');
	}

	/**
	 * Returns MemAvailable value in bytes.
	 *
	 * @return int
	 */
	public function getMemAvailable() {
		return $this->get('MemAvailable');
	}

	/**
	 * Returns Buffers value in bytes.
	 *
	 * @return int
	 */
	public function getBuffers() {
		return $this->get('Buffers');
	}

	/**
	 * Returns Cached value in bytes.
	 *
	 * @return int
	 */
	public function getCached() {
		return $this->get('Cached');
	}

	/**
	 * Returns SwapTotal value in bytes.
	 *
	 * @return int
	 */
	public function getSwapTotal() {
		return $this->get('SwapTotal');
	}

	/**
	 * Returns SwapFree value in bytes.
	 *
	 * @return int
	 */
	public function getSwapFree() {
		return $this->get('SwapFree');
	}

	/**
	 * Returns SwapCached value in bytes.
	 *
	 * @return int
	 */
	public function getSwapCached() {
		return $this->get('SwapCached');
	}

	/**
	 * Returns memory used in bytes, excluding buffered and cached memory.
	 *
	 * @return int
	 *
	 * @throws \RuntimeException if real memory usage can't be computed.
	 */
	public function getRealMemUsed() {
		$total   = $this->getMemTotal();
		$free    = $this->getMemFree();
		$buffers = $this->getBuffers();
		$cached  = $this->getCached();

		if ( in_array(null, [$total, $free, $buffers, $cached], true) ) {
			throw new \RuntimeException('Failed to compute');
		}

		return $total - $free - $buffers - $cached;
	}

	/**
	 * Returns memory free in bytes, including buffered and cached memory.
	 *
	 * @return int
	 *
	 * @throws \RuntimeException if real free memory can't be computed.
	 */
	public function getRealMemFree() {
		$free    = $this->getMemFree();
		$buffers = $this->getBuffers();
		$cached  = $this->getCached();

		if ( in_array(null, [$free, $buffers, $cached], true) ) {
			throw new \RuntimeException('Failed to compute');
		}

		return $free + $buffers + $cached;
	}

	/**
	 * Returns swap used in bytes.
	 *
	 * @return int
	 *
	 * @throws \RuntimeException if swap usage can't be computed.
	 */
	public function getRealSwapUsed() {
		$total = $this->getSwapTotal();
		$free  = $this->getSwapFree();

		if ( in_array(null, [$total, $free], true) ) {
			throw new \RuntimeException('Failed to compute');
		}

		return $total - $free;
	}
}

