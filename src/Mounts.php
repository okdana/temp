<?php

/**
 * This file is part of the Company PHP Library.
 *
 * @copyright © Company. All rights reserved.
 */

namespace Company\SystemInfo\Linux\Proc;

/**
 * Wrapper around /proc/mounts data.
 */
class Mounts extends AbstractProcWrapper {
	/**
	 * @var string Path to mounts file.
	 */
	const MOUNTS_PATH = '/proc/mounts';

	/**
	 * @var \Company\Collection\SimpleObjectCollection Data as collection.
	 */
	protected $collection;

	/**
	 * Object constructor.
	 *
	 * @param string|null $path
	 *   (optional) The path to the meminfo file to be parsed. The default is
	 *   MOUNTS_PATH.
	 *
	 * @return self
	 */
	public function __construct($path = null) {
		$this->path = $path ?? static::MOUNTS_PATH;
		$this->update();
	}

	/**
	 * {@inheritdoc}
	 */
	public function update() {
		if ( ! file_exists($this->path) ) {
			throw new \RuntimeException("No such file: {$this->path}");
		}

		$this->text = trim(file_get_contents($this->path));
		$lines      = explode("\n", $this->text);

		if ( count($lines) < 1 || strpos($this->text, '/') === false ) {
			throw new \RuntimeException("Illegal file contents: {$this->path}");
		}

		foreach ( $lines as $line ) {
			$line   = trim($line);
			$fields = explode(' ', $line);

			if ( count($fields) !== 6 ) {
				continue;
			}

			foreach ( $fields as &$field ) {
				// Decode octal escape sequences like \040
				$field = preg_replace_callback(
					'/\\\(0\d{2})/',
					function ($m) {
						return chr(octdec($m[1]));
					},
					$field
				);
			}

			$this->data[] = (object) [
				'device'     => $fields[0],
				'mountPoint' => $fields[1],
				'fsType'     => $fields[2],
				'options'    => explode(',', $fields[3]),
			];
		}

		$this->collection = new \Company\Collection\SimpleObjectCollection($this->data);

		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function toCollection() {
		return $this->collection;
	}

	/**
	 * Returns a collection of mount entries filtered for the specified device.
	 *
	 * @param string $device
	 *   The device name to filter for.
	 *
	 * @return \Company\Collection\SimpleObjectCollection
	 */
	public function filterForDevice($device) {
		return $this->collection->filterFor(
			function ($element) use ($device) {
				return $element->device === $device;
			}
		);
	}

	/**
	 * Returns a collection of mount entries filtered for the specified mount
	 * point.
	 *
	 * @param string $mountPoint
	 *   The mount point to filter for.
	 *
	 * @return \Company\Collection\SimpleObjectCollection
	 */
	public function filterForMountPoint($mountPoint) {
		return $this->collection->filterFor(
			function ($element) use ($mountPoint) {
				return $element->mountPoint === $mountPoint;
			}
		);
	}

	/**
	 * Returns a collection of mount entries filtered for the specified
	 * file-system type.
	 *
	 * @param string $fsType
	 *   The file-system type to filter for.
	 *
	 * @return \Company\Collection\SimpleObjectCollection
	 */
	public function filterForFSType($fsType) {
		return $this->collection->filterFor(
			function ($element) use ($fsType) {
				return $element->fsType === $fsType;
			}
		);
	}

	/**
	 * Returns a collection of mount entries filtered for the specified option.
	 *
	 * @param string $option
	 *   The option to filter for.
	 *
	 * @return \Company\Collection\SimpleObjectCollection
	 */
	public function filterForOption($option) {
		return $this->collection->filterFor(
			function ($element) use ($option) {
				return in_array($option, $element->options, true);
			}
		);
	}
}

