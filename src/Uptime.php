<?php

/**
 * This file is part of the Company PHP Library.
 *
 * @copyright © Company. All rights reserved.
 */

namespace Company\SystemInfo\Linux\Proc;

/**
 * Wrapper around /proc/uptime data.
 */
class Uptime extends AbstractProcWrapper {
	/**
	 * @var string Path to uptime file.
	 */
	const UPTIME_PATH = '/proc/uptime';

	/**
	 * Object constructor.
	 *
	 * @param string|null $path
	 *   (optional) The path to the uptime file to be parsed. The default is
	 *   UPTIME_PATH.
	 *
	 * @return self
	 */
	public function __construct($path = null) {
		$this->path = $path ?? static::UPTIME_PATH;
		$this->update();
	}

	/**
	 * {@inheritdoc}
	 */
	public function update() {
		if ( ! file_exists($this->path) ) {
			throw new \RuntimeException("No such file: {$this->path}");
		}

		$this->text = trim(file_get_contents($this->path));
		$data       = explode(' ', $this->text);

		if ( count($data) !== 2 ) {
			throw new \RuntimeException("Illegal file contents: {$this->path}");
		}

		$this->data = [
			'upSeconds'   => (float) $data[0],
			'idleSeconds' => (float) $data[1],
		];

		return $this;
	}

	/**
	 * Returns up-time in seconds.
	 *
	 * @return float
	 */
	public function getUpSeconds() {
		return $this->get('upSeconds');
	}

	/**
	 * Returns idle time in seconds.
	 *
	 * @return float
	 */
	public function getIdleSeconds() {
		return $this->get('idleSeconds');
	}

	/**
	 * Returns approximate time of boot.
	 *
	 * @return float
	 */
	public function getUpSince() {
		return time() - $this->getUpSeconds();
	}
}

